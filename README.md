Demo of type hints in Python and surrounding tooling

See mypy catch a type error:

```
pipenv install -d
pipenv run mypy src
```

Generate type signatures with monkeytype:

```
pipenv run monkeytype run no_types_main.py
pipenv run monkeytype stub no_types
```
