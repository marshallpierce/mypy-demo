from typing import Optional, Sequence, TypeVar

T = TypeVar('T')


def first(a: Sequence[T]) -> Optional[T]:
    if len(a) > 0:
        return a[0]
    return None


assert None == first([])
assert 3 == first([3, 4, 5])
